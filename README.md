# Exchange Rate API

## Run Locally

1. Create a `.env` file in the project root with the following variables to connect to your MySQL service:

    ```env
    MYSQL_HOST=mysql_db
    MYSQL_PORT=3306
    MYSQL_DATABASE=your_database_name
    MYSQL_USER=your_database_user
    MYSQL_PASSWORD=your_database_password
    JWT_SECRET=some_secret_key
    ```

2. Run the following command to start the Docker containers:

    ```bash
    docker-compose up -d
    ```

3. To populate the database with sample data, run the following command:

    ```bash
    docker exec nestjs_app npm run seed
    ```

4. The project is running in http://localhost:3000

5. User admin
```
"email": "admin@example.com",
"password": "123456"
```


## Documentation
Visit http://localhost:3000/doc

## Overview

Exchange Rate API is a robust solution for obtaining up-to-date exchange rate information. The project leverages Docker for efficient containerization, MySQL for secure data storage, and NestJS for a powerful backend.

## Technologies Used

- Docker
- MySQL
- NestJS

## API Endpoints

- **POST /auth/login**: Endpoint for user authentication.
- **GET /auth/users**: Retrieve user information.
- **POST /auth/users**: Create a new user.
- **PUT /auth/users/:id**: Update user information.
- **DELETE /auth/users/:id**: Delete a user.
- **GET /exchange-rate**: Retrieve detailed exchange rate information.
- **POST /exchange-rate**: Update exchange rate information.

## Contributing

Contributions are welcomed! Feel free to open issues or submit pull requests to enhance the functionality and features of the Exchange Rate API.

## License

This project is licensed under the [MIT License](LICENSE).

---

*Note: Replace placeholders such as `your_database_name`, `your_database_user`, and `your_database_password` with your actual database credentials.*
