import { Module } from '@nestjs/common';
import { UsersModule } from './modules/users/users.module';
import { AuthModule } from './modules/auth/auth.module';
import { ExchangeRateModule } from './modules/exchange_rates/exchange_rates.module';
import { DbModule } from './modules/db/db.module';

@Module({
  imports: [
    DbModule,
    AuthModule,
    UsersModule,
    ExchangeRateModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
