import { Repository } from "typeorm";
import { UsersService } from "./users.service";
import { User } from "./entities/user.entity";
import { getRepositoryToken } from "@nestjs/typeorm";
import { Test } from "@nestjs/testing";
import * as bcryptjs from 'bcryptjs';
import { BadRequestException } from "@nestjs/common";

describe('UsersService', () => {
    let service: UsersService;
    let userRepository: Repository<User>;
    let userRepositoryMock;

    const USER_REPOSITORY_TOKEN = getRepositoryToken(User);

    beforeEach(async () => {
        userRepositoryMock = {
            findOne: jest.fn(),
            findOneBy: jest.fn(),
            save: jest.fn(),
            find: jest.fn(),
            update: jest.fn(),
            delete: jest.fn(),
        };

        const module = await Test.createTestingModule({
            providers: [
                UsersService,
                {
                    provide: USER_REPOSITORY_TOKEN,
                    useValue: userRepositoryMock
                }
            ]
        }).compile();

        service = module.get<UsersService>(UsersService);
        userRepository = module.get<Repository<User>>(USER_REPOSITORY_TOKEN);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('should userRepository be defined', () => {
        expect(userRepository).toBeDefined();
    });

    describe('create', () => {
        it('should create a user', async () => {
          const createUserDto = { email: 'test@example.com', password: 'password', name: 'Test User', role: 'user' };
    
            userRepositoryMock.findOneBy.mockResolvedValue(null);
            userRepositoryMock.findOne.mockResolvedValue({
                id: 1,
                ...createUserDto
            
            });
            
            userRepositoryMock.save.mockResolvedValue({
                id: 1,
                ...createUserDto,
                password: bcryptjs.hashSync(createUserDto.password, 10),
            });
        

            expect(await service.create(createUserDto)).toEqual({
                id: 1,
                ...createUserDto
            });
        });
    
        it('should throw BadRequestException if user already exists', async () => {
          const createUserDto = { email: 'test@example.com', password: 'password', name: 'Test User', role: 'user' };
    
          userRepositoryMock.findOneBy.mockResolvedValue({});
    
          await expect(service.create(createUserDto)).rejects.toThrowError(BadRequestException);
        });
    });
    
    describe('update', () => {
        it('should update a user', async () => {
          const userId = 1;
          const updateUserDto = { email: 'updated@example.com', password: 'updatedpassword', name: 'Updated User', role: 'user' };
    
          userRepositoryMock.findOne.mockResolvedValue(null);
    
          await service.update(userId, updateUserDto);
    
          expect(userRepositoryMock.update).toHaveBeenCalledWith(
            { id: userId },
            { ...updateUserDto, password: expect.any(String) },
          );
        });
    
        it('should throw BadRequestException if updated email already exists', async () => {
          const userId = 1;
          const updateUserDto = { email: 'existing@example.com', password: 'updatedpassword', name: 'Updated User', role: 'user' };
    
          userRepositoryMock.findOne.mockResolvedValue(updateUserDto);
    
          await expect(service.update(userId, updateUserDto)).rejects.toThrowError(BadRequestException);
        });
    });
    
    describe('remove', () => {
        it('should remove a user', async () => {
          const userId = 1;
          const user = { id: userId, role: 'user' };
    
          userRepositoryMock.findOne.mockResolvedValue(1);
          userRepositoryMock.delete.mockResolvedValue(user);
    
          const result = await service.remove(userId);
    
          expect(userRepositoryMock.delete).toHaveBeenCalledWith(userId);
          expect(result).toEqual({ message: 'User deleted' });
        });
    
        it('should throw BadRequestException if user is an admin', async () => {
          const adminId = 1;
          const admin = { id: adminId, role: 'admin' };
    
          userRepositoryMock.findOne.mockResolvedValue(admin);
    
          await expect(service.remove(adminId)).rejects.toThrowError(BadRequestException);
        });
    
        it('should throw BadRequestException if user not found', async () => {
          const userId = 1;
    
          userRepositoryMock.findOne.mockResolvedValue(null);
    
          await expect(service.remove(userId)).rejects.toThrowError(BadRequestException);
        });
    });
});