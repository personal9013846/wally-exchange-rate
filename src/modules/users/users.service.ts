import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Not, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import * as bcryptjs from 'bcryptjs';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const user = await this.userRepository.findOneBy({ email: createUserDto.email});

    if (user) {
      throw new BadRequestException('User already exists');
    }

    const userCreated = await this.userRepository.save({
      ...createUserDto,
      password: await bcryptjs.hash(createUserDto.password, 10),
    });

    return this.findOne(userCreated.id);
  }

  findOneByEmail(email: string) {
    return this.userRepository.findOneBy({ email });
  }

  findByEmailWithPassword(email: string) {
    return this.userRepository.findOne({
      where: { email },
      select: ['id', 'name', 'email', 'password', 'role'],
    });
  }

  findAll() {
    return this.userRepository.find();
  }

  findOne(id: number) {
    return this.userRepository.findOne({
      where: { id },
      select: ['id', 'name', 'email', 'role'],
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.userRepository.findOne({
      where: { email: updateUserDto.email, id: Not(id) },
    });

    if (user) {
      throw new BadRequestException('User already exists');
    }

    await this.userRepository.update(
      { id },
      {
        ...updateUserDto,
        password: bcryptjs.hashSync(updateUserDto.password, 10),
      },
    );

    return this.findOne(id);
  }

  async remove(id: number) {
    const user = await this.findOne(id);
    if (!user) {
      throw new BadRequestException('User not found');
    }

    if(user.role === 'admin') {
      throw new BadRequestException('User is admin');
    }

    await this.userRepository.delete(id);

    return { message: 'User deleted' };
  }
}
