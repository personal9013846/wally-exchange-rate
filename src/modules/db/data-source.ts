import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { config } from 'dotenv';
import { SeederOptions } from 'typeorm-extension';
import { DataSource, DataSourceOptions } from 'typeorm';
import ApplicationSeeder from './seeds/seeder';

config();

export const dataSourceOptions: TypeOrmModuleOptions & SeederOptions = {
  type: 'mysql',
  host: process.env.MYSQL_HOST,
  port: +process.env.MYSQL_PORT,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  autoLoadEntities: true,
  synchronize: true,
  entities: [__dirname + '/../**/*.entity.ts'],
  seeds: [
    ApplicationSeeder
  ],
};

export const source = new DataSource(
  dataSourceOptions as DataSourceOptions & SeederOptions,
);

export default source;