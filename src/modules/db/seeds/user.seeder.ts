import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { DataSource } from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { Role } from '../../../common/enums/rol.enum';
import * as bcryptjs from 'bcryptjs';

export default class UserSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<void> {
    await dataSource.query('TRUNCATE TABLE users;');

    const repository = dataSource.getRepository(User);
    await repository.insert({
        name: 'admin',
        email: 'admin@example.com',
        password: await bcryptjs.hash('123456', 10),
        role: Role.ADMIN
    });
  }
}
