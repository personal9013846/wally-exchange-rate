
import { Seeder, SeederFactoryManager, runSeeders } from 'typeorm-extension';
import { DataSource } from 'typeorm';
import ExchangeRateSeeder from './exchange_rate.seeder';
import UserSeeder from './user.seeder';

export default class ApplicationSeeder implements Seeder {
    public async run(dataSource: DataSource, factoryManager: SeederFactoryManager): Promise<void> {
    
      await runSeeders(dataSource, {
        seeds: [UserSeeder, ExchangeRateSeeder],
        factories: [],
      });
    }
}