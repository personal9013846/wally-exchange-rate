import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { DataSource } from 'typeorm';
import { ExchangeRate } from '../../exchange_rates/entities/exchange_rate.entity';

export default class ExchangeRateSeeder implements Seeder {
  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<void> {
    await dataSource.query('TRUNCATE TABLE exchange_rates;');

    const repository = dataSource.getRepository(ExchangeRate);

    const exchangeRates = [
      {
        source_currency: 'USD',
        target_currency: 'EUR',
        exchange_rate: 0.85,
      },
      {
        source_currency: 'EUR',
        target_currency: 'USD',
        exchange_rate: 1.18,
      },
      {
        source_currency: 'USD',
        target_currency: 'PEN',
        exchange_rate: 3.98,
      },
      {
        source_currency: 'PEN',
        target_currency: 'USD',
        exchange_rate: 0.25,
      },
      {
        source_currency: 'EUR',
        target_currency: 'PEN',
        exchange_rate: 4.68,
      },
      {
        source_currency: 'PEN',
        target_currency: 'EUR',
        exchange_rate: 0.21,
      },
    ];

    await repository.save(exchangeRates);
  }
}
