import { Test, TestingModule } from '@nestjs/testing';
import { BadRequestException, HttpException, HttpStatus } from '@nestjs/common';
import { ExchangeRatesService } from './exchange_rates.service';
import { CalculateExchangeRateDto } from './dto/calculate_exchange_rate.dto';
import { UpdateExchangeRateDto } from './dto/update_exchange_rate.dto';

class MockExchangeRateStrategy {
  calculateExchangeRate(sourceCurrency: string, targetCurrency: string): Promise<number> {
    return Promise.resolve(1.5); // Valor de cambio arbitrario para las pruebas
  }

  updateExchangeRate(sourceCurrency: string, targetCurrency: string, exchangeRateValue: number): Promise<any> {
    return Promise.resolve({
      source_currency: sourceCurrency,
      target_currency: targetCurrency,
      exchange_rate: exchangeRateValue,
    });
  }
}

describe('ExchangeRatesService', () => {
  let service: ExchangeRatesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ExchangeRatesService,
        {
          provide: 'ExchangeRateStrategy',
          useClass: MockExchangeRateStrategy,
        },
      ],
    }).compile();

    service = module.get<ExchangeRatesService>(ExchangeRatesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('calculateExchangeRate', () => {
    it('should calculate exchange rate successfully', async () => {
      const calculateExchangeRateDto: CalculateExchangeRateDto = {
        source_currency: 'USD',
        target_currency: 'EUR',
        amount: 100,
      };

      const result = await service.calculateExchangeRate(calculateExchangeRateDto);

      expect(result).toEqual({
        amount_original: 100,
        amount_converted: 150,
        source_currency: 'USD',
        target_currency: 'EUR',
        exchange_rate: 1.5,
      });
    });

    it('should throw BadRequestException for missing parameters', async () => {
      const calculateExchangeRateDto: CalculateExchangeRateDto = {
        source_currency: 'USD',
        target_currency: 'EUR',
        amount: undefined,
      };

      await expect(service.calculateExchangeRate(calculateExchangeRateDto)).rejects.toThrowError(
        BadRequestException,
      );
    });

    it('should throw BadRequestException for identical source and target currencies', async () => {
      const calculateExchangeRateDto: CalculateExchangeRateDto = {
        source_currency: 'USD',
        target_currency: 'USD',
        amount: 100,
      };

      await expect(service.calculateExchangeRate(calculateExchangeRateDto)).rejects.toThrowError(
        BadRequestException,
      );
    });

    it('should throw HttpException for other errors', async () => {
      const calculateExchangeRateDto: CalculateExchangeRateDto = {
        source_currency: 'USD',
        target_currency: 'EUR',
        amount: 100,
      };

      jest.spyOn(MockExchangeRateStrategy.prototype, 'calculateExchangeRate').mockRejectedValue(new Error('Some error'));

      await expect(service.calculateExchangeRate(calculateExchangeRateDto)).rejects.toThrowError(
        HttpException,
      );
    });
  });

  describe('updateExchangeRate', () => {
    it('should update exchange rate successfully', async () => {
      const updateExchangeRateDto: UpdateExchangeRateDto = {
        source_currency: 'USD',
        target_currency: 'EUR',
        exchange_rate_value: 1.8,
      };

      const result = await service.updateExchangeRate(updateExchangeRateDto);

      expect(result).toEqual({
        source_currency: 'USD',
        target_currency: 'EUR',
        exchange_rate: 1.8,
      });
    });

    it('should throw BadRequestException for missing parameters', async () => {
      const updateExchangeRateDto: UpdateExchangeRateDto = {
        source_currency: 'USD',
        target_currency: 'EUR',
        exchange_rate_value: undefined,
      };

      await expect(service.updateExchangeRate(updateExchangeRateDto)).rejects.toThrowError(
        BadRequestException,
      );
    });

    it('should throw HttpException for other errors', async () => {
      const updateExchangeRateDto: UpdateExchangeRateDto = {
        source_currency: 'USD',
        target_currency: 'EUR',
        exchange_rate_value: 1.8,
      };

      jest.spyOn(MockExchangeRateStrategy.prototype, 'updateExchangeRate').mockRejectedValue(new Error('Some error'));

      await expect(service.updateExchangeRate(updateExchangeRateDto)).rejects.toThrowError(
        HttpException,
      );
    });
  });
});
