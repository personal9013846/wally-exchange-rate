import { Test, TestingModule } from '@nestjs/testing';
import { UpdateExchangeRateDto } from './dto/update_exchange_rate.dto';
import { ExchangeRatesController } from './exchange_rates.controller';
import { ExchangeRatesService } from './exchange_rates.service';
import { JwtService } from '@nestjs/jwt';

class MockExchangeRatesService {
  calculateExchangeRate(query: any) {
    return Promise.resolve({
      amount_original: query.amount,
      amount_converted: query.amount * 1.5, // Valor de cambio arbitrario para las pruebas
      source_currency: query.source_currency,
      target_currency: query.target_currency,
      exchange_rate: 1.5,
    });
  }

  updateExchangeRate(updateExchangeRateDto: UpdateExchangeRateDto) {
    return Promise.resolve({
      source_currency: updateExchangeRateDto.source_currency,
      target_currency: updateExchangeRateDto.target_currency,
      exchange_rate: updateExchangeRateDto.exchange_rate_value,
    });
  }
}

describe('ExchangeRatesController', () => {
  let controller: ExchangeRatesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExchangeRatesController],
      providers: [
        {
          provide: ExchangeRatesService,
          useClass: MockExchangeRatesService,
        },
        {
          provide: JwtService,
          useValue: {
              signAsync: jest.fn(),
          },
      },
      ],
    }).compile();

    controller = module.get<ExchangeRatesController>(ExchangeRatesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('calculateExchangeRate', () => {
    it('should calculate exchange rate successfully', async () => {
      const query = { source_currency: 'USD', target_currency: 'EUR', amount: 100 };
      const result = await controller.calculateExchangeRate(query);

      expect(result).toEqual({
        amount_original: 100,
        amount_converted: 150,
        source_currency: 'USD',
        target_currency: 'EUR',
        exchange_rate: 1.5,
      });
    });
  });

  describe('update', () => {
    it('should update exchange rate successfully', async () => {
      const updateExchangeRateDto: UpdateExchangeRateDto = {
        source_currency: 'USD',
        target_currency: 'EUR',
        exchange_rate_value: 1.8,
      };

      const result = await controller.update(updateExchangeRateDto);

      expect(result).toEqual({
        source_currency: 'USD',
        target_currency: 'EUR',
        exchange_rate: 1.8,
      });
    });
  });
});
