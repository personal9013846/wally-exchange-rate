import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ExchangeRate } from './entities/exchange_rate.entity';
import { ExchangeRatesController } from './exchange_rates.controller';
import { ExchangeRatesService } from './exchange_rates.service';
import { FixedRateStrategy } from './strategy/fixed-rate.strategy';
import { RealTimeRateStrategy } from './strategy/real-time-rate.strategy';

@Module({
  imports: [TypeOrmModule.forFeature([ExchangeRate])],
  controllers: [ExchangeRatesController],
  providers: [
    ExchangeRatesService,
    { provide: 'ExchangeRateStrategy', useClass: FixedRateStrategy }
  ],
  exports: [ExchangeRatesService],
})
export class ExchangeRateModule {}
