import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { Auth } from '../../modules/auth/decorators/auth.decorator';
import { Role } from '../../common/enums/rol.enum';
import { ExchangeRatesService } from './exchange_rates.service';
import { UpdateExchangeRateDto } from './dto/update_exchange_rate.dto';
import { ApiBearerAuth, ApiTags, ApiParam, ApiResponse } from '@nestjs/swagger';
import { CalculateExchangeRateResponseDto } from './dto/calculate_exchange_rate_response.dto';
import { CalculateExchangeRateDto } from './dto/calculate_exchange_rate.dto';

@ApiTags('exchange-rates')
@ApiBearerAuth()
@Auth(Role.USER)
@Controller('exchange-rates')
export class ExchangeRatesController {
  constructor(private readonly exchangeRatesService: ExchangeRatesService) {}

  @Get()
  @ApiParam({ name: 'source_currency', type: String })
  @ApiParam({ name: 'target_currency', type: String})
  @ApiParam({ name: 'amount', type: Number})
  @ApiResponse({ status: 200, description: 'Operación exitosa', type: CalculateExchangeRateResponseDto })
  calculateExchangeRate(@Query() query: CalculateExchangeRateDto) {
    const { source_currency, target_currency, amount } = query;
    return this.exchangeRatesService.calculateExchangeRate({ source_currency, target_currency, amount });
  }

  @Post()
  update(@Body() calculateExchangeRateDto: UpdateExchangeRateDto) {
    return this.exchangeRatesService.updateExchangeRate(calculateExchangeRateDto);
  }
}
