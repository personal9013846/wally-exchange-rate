export interface ExchangeRateStrategy {
    calculateExchangeRate(source_currency: string, target_currency: string): Promise<number>;
    updateExchangeRate(source_currency: string, target_currency: string, exchange_rate_value: number): Promise<object>;
}
  