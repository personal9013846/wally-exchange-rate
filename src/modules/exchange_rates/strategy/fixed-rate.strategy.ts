import { InjectRepository } from '@nestjs/typeorm';
import { ExchangeRateStrategy } from './exchange-rate.strategy';
import { ExchangeRate } from '../entities/exchange_rate.entity';
import { Repository } from 'typeorm';
import { NotFoundException } from '@nestjs/common';

export class FixedRateStrategy implements ExchangeRateStrategy {
    constructor(
        @InjectRepository(ExchangeRate)
        private readonly exchangueRepository: Repository<ExchangeRate>,
    ) {}

  async calculateExchangeRate(source_currency: string, target_currency: string): Promise<number> {
    const exchange_rate = await this.exchangueRepository.findOne({
        where: { source_currency, target_currency },
    });

    if (!exchange_rate) {
        throw new NotFoundException(`Exchange rate not found for ${source_currency} to ${target_currency}`);
    }

    return exchange_rate.exchange_rate;
  }

  async updateExchangeRate(source_currency: string, target_currency: string, exchange_rate_value: number): Promise<object> {
    const exchange_rate = await this.exchangueRepository.findOne({
        where: { source_currency, target_currency },
    });

    if (!exchange_rate) {
        throw new NotFoundException(`Exchange rate not found for ${source_currency} to ${target_currency}`);
    }

    exchange_rate.exchange_rate = exchange_rate_value;

    await this.exchangueRepository.save(exchange_rate);

    return {
        source_currency,
        target_currency,
        exchange_rate: exchange_rate_value,
    };
  }
}
