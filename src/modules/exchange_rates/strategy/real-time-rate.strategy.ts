import { ExchangeRateStrategy } from './exchange-rate.strategy';

export class RealTimeRateStrategy implements ExchangeRateStrategy {
    async calculateExchangeRate(source_currency: string, target_currency: string): Promise<number> {
        return 0;
    }

    async updateExchangeRate(source_currency: string, target_currency: string, exchange_rate_value: number): Promise<object> {
        return {
            source_currency,
            target_currency,
            exchange_rate: exchange_rate_value,
        }
    }
}
