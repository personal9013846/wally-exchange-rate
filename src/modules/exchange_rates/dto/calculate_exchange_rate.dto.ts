import { Transform } from 'class-transformer';
import { IsString, IsNumber, Length} from 'class-validator';

export class CalculateExchangeRateDto {
    @Transform(({ value }) => value.trim())
    @Length(3, 3)
    @IsString()
    source_currency: string;

    @Transform(({ value }) => value.trim())
    @Length(3, 3)
    @IsString()
    target_currency: string;

    @Transform(({ value }) => +value)
    @IsNumber()
    amount: number;
}