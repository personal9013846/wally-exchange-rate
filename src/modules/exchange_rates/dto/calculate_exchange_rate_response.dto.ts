export class CalculateExchangeRateResponseDto {
    amount_original: number;
    amount_converted: number;
    source_currency: string;
    target_currency: string;
    exchange_rate: number;
}