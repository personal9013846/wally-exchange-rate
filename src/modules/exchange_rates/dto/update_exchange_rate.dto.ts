import { Transform } from 'class-transformer';
import { IsString, IsNumber, Length } from 'class-validator';

export class UpdateExchangeRateDto {
    @Transform(({ value }) => value.trim())
    @Length(3, 3)
    @IsString()
    source_currency: string;

    @Transform(({ value }) => value.trim())
    @Length(3, 3)
    @IsString()
    target_currency: string;

    @IsNumber()
    exchange_rate_value: number;
}