import { BadRequestException, HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { CalculateExchangeRateDto } from './dto/calculate_exchange_rate.dto';
import { UpdateExchangeRateDto } from './dto/update_exchange_rate.dto';
import { ExchangeRateStrategy } from './strategy/exchange-rate.strategy';

@Injectable()
export class ExchangeRatesService {
  constructor(
    @Inject('ExchangeRateStrategy')
    private readonly exchangeRateStrategy: ExchangeRateStrategy,
  ) {}


    async calculateExchangeRate(calculateExchangeRateDto: CalculateExchangeRateDto) {
        try {
            const { source_currency, target_currency, amount } = calculateExchangeRateDto;

            if(!source_currency || !target_currency || !amount) {
                throw new BadRequestException("Source currency, target currency and amount are required");
            }

            if (source_currency === target_currency) {
                throw new BadRequestException("Source currency and target currency can't be the same");
            }

            const exchange_rate = await this.exchangeRateStrategy.calculateExchangeRate(source_currency, target_currency);

            const amount_converted = +(amount * exchange_rate).toFixed(2);

            return {
                amount_original: +amount,
                amount_converted,
                source_currency,
                target_currency,
                exchange_rate: +exchange_rate,
            };
        } catch (error) {
            if (error instanceof HttpException) {
                throw error;
            }

            throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    async updateExchangeRate(updateExchangeRateDto: UpdateExchangeRateDto) {
        try {
            const { source_currency, target_currency, exchange_rate_value } = updateExchangeRateDto;

            if(!source_currency || !target_currency || !exchange_rate_value) {
                throw new BadRequestException("Source currency, target currency and exchange rate value are required");
            }

            const update_exchange_rate = await this.exchangeRateStrategy.updateExchangeRate(source_currency, target_currency, exchange_rate_value);

            return update_exchange_rate;

        } catch (error) {
            if (error instanceof HttpException) {
                throw error;
            }

            throw new HttpException("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
