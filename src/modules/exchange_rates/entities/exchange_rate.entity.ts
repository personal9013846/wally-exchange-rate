import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    Unique
  } from 'typeorm';
  
  @Entity('exchange_rates') 
  @Unique(['source_currency', 'target_currency'])
  export class ExchangeRate {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    source_currency: string;

    @Column()
    target_currency: string;

    @Column({ type: 'decimal', precision: 12, scale: 2 })
    exchange_rate: number;
  }
  